#Práctica 6. Desarrollo dirigido por pruebas

##Introducción
El objetivo de esta práctica es familiarizarnos con el método de trabajo asistido por pruebas **TDD** con el uso de la herramiento rspec 
y la creación de la estructura de trabajo en ruby con **bundler**


##Práctica realizada por:
        Eric Ramos Suárez
        José Lucas Ruiz González